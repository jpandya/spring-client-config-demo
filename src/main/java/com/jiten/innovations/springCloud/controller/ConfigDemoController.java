package com.jiten.innovations.springCloud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfigDemoController {
	
	@Autowired 
	Environment environment;
	
	@GetMapping("/getGreetingMessage")
	public String getGreetingMessage(){
		return environment.getProperty("greetingMsg");
	}
}
