package com.jiten.innovations.springCloud.SpringClientConfigDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.jiten.innovations.springCloud.controller"})
public class SpringClientConfigDemoApplication {

	
	public static void main(String[] args) {
		SpringApplication.run(SpringClientConfigDemoApplication.class, args);
	}

}
